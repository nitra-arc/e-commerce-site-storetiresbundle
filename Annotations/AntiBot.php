<?php

namespace Nitra\StoreBundle\Annotations;

/**
 * @Annotation
 */
class AntiBot
{
    public $check = true;

    /**
     * @return bool
     */
    public function checkIsEnabled()
    {
        return $this->check == true;
    }
}