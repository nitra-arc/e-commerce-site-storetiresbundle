<?php

namespace Nitra\StoreBundle\Controller\Imagine;

use Liip\ImagineBundle\Controller\ImagineController as BaseImagineController;
use Liip\ImagineBundle\Exception\Imagine\Filter\NonExistingFilterException;
use Liip\ImagineBundle\Exception\Binary\Loader\NotLoadableException;
use Imagine\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nitra\StoreBundle\Lib\Globals;

class ImagineController extends BaseImagineController
{
    /**
     * {@inheritdoc}
     */
    public function filterAction(Request $request, $path, $filter)
    {
        try {
            if (!$this->cacheManager->isStored($path, $filter)) {
                try {
                    $binary = $this->dataManager->find($filter, $path);

                    $this->addWatermark($filter);
                } catch (NotLoadableException $e) {
                    if ($defaultImageUrl = $this->dataManager->getDefaultImageUrl($filter)) {
                        return new RedirectResponse($defaultImageUrl);
                    }

                    throw new NotFoundHttpException('Source image could not be found', $e);
                }

                $this->cacheManager->store(
                    $this->filterManager->applyFilter($binary, $filter),
                    $path,
                    $filter
                );
            }

            return new RedirectResponse($this->cacheManager->resolve($path, $filter), 301);
        } catch (NonExistingFilterException $e) {
            $message = sprintf('Could not locate filter "%s" for path "%s". Message was "%s"', $filter, $path, $e->getMessage());

            if (null !== $this->logger) {
                $this->logger->debug($message);
            }

            throw new NotFoundHttpException($message, $e);
        } catch (RuntimeException $e) {
            throw new \RuntimeException(sprintf('Unable to create image for path "%s" and filter "%s". Message was "%s"', $path, $filter, $e->getMessage()), 0, $e);
        }
    }

    /**
     * Set watermark image from store if specified or unset watermark filter else
     *
     * @param string $filter    Filter name
     */
    protected function addWatermark($filter)
    {
        $filterConfig = $this->filterManager->getFilterConfiguration();
        $config       = $filterConfig->get($filter);
        $store        = Globals::getStore();

        if (isset($config['filters']['watermark'])) {
            // if watermark
            if ($store['watermark']) {
                $config['filters']['watermark']['image'] = $store['watermark'];
            } else {
                unset($config['filters']['watermark']);
            }
        }

        $filterConfig->set($filter, $config);
    }
}