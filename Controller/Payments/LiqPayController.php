<?php

namespace Nitra\StoreBundle\Controller\Payments;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class LiqPayController extends NitraController
{
    /**
     * namespace of form type
     * @var string
     */
    protected $form = '\Nitra\StoreBundle\Form\Type\Payments\LiqPayType';

    /**
     * action of form
     * @var string
     */
    protected $action = 'https://www.liqpay.com/api/pay';

    /**
     * @return array
     */
    protected function getConfiguration()
    {
        $store = $this->getStore();

        return key_exists('liq_pay', $store['payments'])
            ? $store['payments']['liq_pay']
            : array();
    }

    /**
     * @Template("NitraStoreBundle:Payments:form.html.twig")
     */
    public function formAction()
    {
        $configs = $this->getConfiguration();

        // if not on of configs or private key in configs or public key in configs
        if (!$configs || !key_exists('private_key', $configs) || !key_exists('public_key', $configs)) {
            // return empty respomse
            return new Response();
        }

        $options = $this->getFormOptions($configs);

        $options['signature'] = $this->signature($configs, $options);

        $form = $this->createForm(new $this->form, null, array(
            'action'    => $this->action,
            'hiddens'   => $options,
            'attr'      => array(
                'accept-charset'    => 'utf-8',
            ),
        ));

        return array(
            'form'      => $form->createView(),
        );
    }

    /**
     * get options of form
     * 
     * @param array $configs
     * 
     * @return array
     */
    protected function getFormOptions($configs)
    {
        return array(
            'public_key'    => $configs['public_key'],
            'amount'        => $this->getOrderAmount(),
            'currency'      => $this->getOrderCurrency(),
            'description'   => $this->getOrderDescription(),
            'order_id'      => $this->getOrderId(),
            'result_url'    => $this->generateUrl('nitra_store_home_index'),
            'type'          => 'buy',
            'sandbox'       => true,
        );
    }

    /**
     * get total order price
     * @return float
     */
    protected function getOrderAmount()
    {
        return (float) $this->getRequest()->getSession()->get('last_order_total_summ');
    }

    /**
     * get order currency
     * @return string
     */
    protected function getOrderCurrency()
    {
        return 'UAH';
    }

    /**
     * get order id from session
     * @return string
     */
    protected function getOrderId()
    {
        return $this->getRequest()->getSession()->get('last_order_id');
    }

    /**
     * get description for pay
     * @return string
     */
    protected function getOrderDescription()
    {
        $store = $this->getStore();

        return $this->getTranslator()->trans('payments.pay_title', array(
            '%orderId%'     => $this->getOrderId(),
            '%storeName%'   => $store['name'],
            '%storeHost%'   => $store['host'],
        ), 'NitraStoreBundle');
    }

    /**
     * format set product description to pay
     * 
     * @param \Nitra\ProductBundle\Document\Product $product
     * 
     * @return string
     */
    protected function formatSetProductDescription($product)
    {
        return $product->getFullName();
    }

    /**
     * create signature
     * 
     * @param array $configs
     * @param array $options
     * 
     * @return string
     */
    protected function signature($configs, $options)
    {
        $implodedData = implode('', array(
            'private_key'   => $configs['private_key'],
            'amount'        => $options['amount'],
            'currency'      => $options['currency'],
            'public_key'    => $configs['public_key'],
            'order_id'      => $options['order_id'],
            'type'          => $options['type'],
            'description'   => $options['description'],
            'result_url'    => $options['result_url'],
            'server_url'    => '',
        ));

        return base64_encode(sha1($implodedData, true));
    }
}