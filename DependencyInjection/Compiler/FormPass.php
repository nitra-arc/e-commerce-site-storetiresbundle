<?php

namespace Nitra\StoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FormPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $resources              = $container->getParameter('twig.form.resources');
        $symfony2Resources      = array_slice($resources, 0, 1);
        $additionalResources    = array_slice($resources, 1);
        
        $newResources = array(
            'NitraStoreBundle:Form:fields.html.twig',
            'NitraStoreBundle:Form:maskedInput.html.twig'
        );

        $container->setParameter('twig.form.resources', array_merge($symfony2Resources, $newResources, $additionalResources));
    }
}