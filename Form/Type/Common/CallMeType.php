<?php

namespace Nitra\StoreBundle\Form\Type\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class CallMeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'required'              => false, 
            'label'                 => 'callMe.name.label',
            'help'                  => 'callMe.name.help',
            'translation_domain'    => 'NitraStoreBundle',
            'constraints'           => array(
                new Constraints\Regex('/^[a-zа-яёґєії. ]{1,255}$/ium'),
            )
        ));
        $builder->add('phone', 'masked_input', array(
            'mask'                  => '+38(999)999-99-99',
            'required'              => true,
            'label'                 => 'callMe.phone.label',
            'help'                  => 'callMe.phone.help',
            'constraints'           => array(
                new Constraints\NotBlank(),
            )
        ));
        
        $builder->add('comment', 'textarea', array(
            'required'              => false,
            'label'                 => 'callMe.comment.label',
            'help'                  => 'callMe.comment.help',
        ));
        
        $builder->add('input', 'button', array(
            'label'                 => 'callMe.button', 
        ));
    }

    public function getName()
    {
        return 'callMe';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }
}