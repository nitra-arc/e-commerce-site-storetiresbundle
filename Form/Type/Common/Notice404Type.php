<?php

namespace Nitra\StoreBundle\Form\Type\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class Notice404Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'required'              => true, 
            'label'                 => 'notice404.name.label',
            'help'                  => 'notice404.name.help',
            'constraints'           => array(
                new Constraints\NotBlank(),
                new Constraints\Regex('/^[a-zа-яёґєії. ]{1,255}$/ium'),
            )
        ));
        
        $builder->add('phone', 'masked_input', array(
            'mask'                  => '+38(999)999-99-99',
            'required'              => true,
            'label'                 => 'notice404.email',
            'help'                  => 'notice404.email_phone',
            'constraints'           => array(
                new Constraints\NotBlank(),
            )
        ));
        
        $builder->add('comment', 'textarea', array(
            'required'              => false, 
            'label'                 => 'notice404.comment.label',
            'help'                  => 'notice404.comment.help',
        ));
        
        $builder->add('input', 'button', array(
            'label'                 => 'notice404.button', 
        ));
        $builder->add('link', 'hidden');
    }

    public function getName()
    {
        return 'notice_404';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }
}