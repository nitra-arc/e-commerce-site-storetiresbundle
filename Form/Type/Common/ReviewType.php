<?php

namespace Nitra\StoreBundle\Form\Type\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array(
            'required'              => false, 
            'label'                 => 'review.name.label',
            'help'                  => 'review.name.help',
            'constraints'           => array(
                new Constraints\Regex('/^[a-zа-яёґєії. ]{1,255}$/ium'),
            )
        ));
        
        $builder->add('phone', 'masked_input', array(
            'mask'                  => '+38(999)999-99-99',
            'required'              => false,
            'label'                 => 'review.phone.label',
            'help'                  => 'review.phone.help',
        ));
        
        $builder->add('comment', 'textarea', array(
            'required'              => true, 
            'label'                 => 'review.comment.label',
            'help'                  => 'review.comment.help',
            'constraints'           => array(
                new Constraints\NotBlank(),
            )
        ));
        
        $builder->add('send', 'button', array(
            'label'                 => 'review.button', 
        ));
    }

    public function getName()
    {
        return 'review';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraStoreBundle',
        ));
    }
}