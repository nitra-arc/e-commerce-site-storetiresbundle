<?php

namespace Nitra\StoreBundle\Form\Type\Payments;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LiqPayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->addHiddenFields($builder, $options['hiddens']);

        $this->addButtonSubmit($builder);
    }

    protected function addButtonSubmit(FormBuilderInterface $builder)
    {
        $builder->add('save', 'submit', array(
            'label' => 'payments.submit',
        ));
    }

    protected function addHiddenFields(FormBuilderInterface $builder, $hiddens)
    {
        foreach ($hiddens as $name => $value) {
            $builder->add($name, 'hidden', array(
                'data'  => $value,
            ));
        }
    }

    public function getName()
    {
        return '';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain'    => 'NitraStoreBundle',
            'attr'                  => array(
                'id'                    => 'liq_pay_form',
            ),
            'hiddens'               => array(),
        ));
    }
}