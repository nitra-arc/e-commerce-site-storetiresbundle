<?php

namespace Nitra\StoreBundle\Helper;

use Nitra\StoreBundle\Lib\Globals;

/**
 * @author     Nitra Labs
 */
class nlActions
{
    /**
     * Get discount for product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return int
     */
    public static function getProductDiscount($product)
    {
        $store = Globals::getStore();
        $productStorePrice  = $product->getStorePrice();
        $originalPrice       = isset($productStorePrice[$store['id']]['price'])
            ? $productStorePrice[$store['id']]['price']
            : 0;
        $productDiscount = self::getOriginalProductDiscount($product);
        $actionDiscount  = self::getDiscountByProductId($product->getId(), $originalPrice);

        $resultDiscount = $actionDiscount > $productDiscount
            ? $actionDiscount
            : $productDiscount;

        if ($resultDiscount > 100) {
            $resultDiscount = 100;
        } elseif ($resultDiscount < 0) {
            $resultDiscount = 0;
        }

        return $resultDiscount;
    }

    /**
     * Get product discount by id
     *
     * @param string $id            Product id
     * @param float  $originalPrice Original product price from database (without any discount)
     *
     * @return integer Product discount in percent
     */
    public static function getDiscountByProductId($id, $originalPrice)
    {
        $discount  = 0;
        $container = Globals::$container;
        if ($container && array_key_exists('NitraActionManagementBundle', $container->getParameter('kernel.bundles'))) {
            $store              = Globals::getStore();
            $cache              = $container->get('cache_apc');
            $cacheKey           = $container->getParameter('mongo_database_name') . '_nitra_actions_by_product_ids_' . $store['host'];
            if ($cache->contains($cacheKey)) {
                $action             = $cache->fetch($cacheKey);
                $aDiscount          = (array_key_exists($id, $action) && array_key_exists('discount', $action[$id]))
                    ? $action[$id]['discount']
                    : 0;
                $aDiscountType      = (array_key_exists($id, $action) && array_key_exists('type', $action[$id]))
                    ? $action[$id]['type']
                    : 'percent';

                switch ($aDiscountType) {
                    case 'summ':
                        $discount = 100 * $aDiscount / $originalPrice;
                        break;
                    default:
                        $discount = $aDiscount;
                        break;
                }
            }
        }

        return $discount;
    }

    /**
     * Get product discount
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return int
     */
    protected static function getOriginalProductDiscount($product)
    {
        $storeId    = Globals::getStore()['id'];
        $storePrice = $product->getStorePrice();

        return array_key_exists($storeId, $storePrice) && array_key_exists('discount', $storePrice[$storeId])
            ? $storePrice[$storeId]['discount']
            : 0;
    }

    /**
     * Get product action
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return \Nitra\ActionManagementBundle\Document\Action|bool
     */
    public static function getProductAction($product)
    {
        $container = Globals::$container;
        if (!$container || !array_key_exists('NitraActionManagementBundle', $container->getParameter('kernel.bundles'))) {
            return false;
        }

        $store              = Globals::getStore();
        $cache              = $container->get('cache_apc');
        $cacheKey           = $container->getParameter('mongo_database_name') . '_nitra_actions_by_product_ids_' . $store['host'];
        if ($cache->contains($cacheKey)) {
            $action         = $cache->fetch($cacheKey);
            $actionId       = array_key_exists($product->getId(), $action)
                ? $action[$product->getId()]['id']
                : false;
            if ($actionId) {
                $dm = $container->get('doctrine_mongodb.odm.document_manager');
                return $dm->find('NitraActionManagementBundle:Action', $actionId);
            }
        }
        return false;
    }
}