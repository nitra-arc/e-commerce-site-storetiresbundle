<?php

namespace Nitra\StoreBundle\Imagine\Filter;

use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;
use Imagine\Filter\Basic\Thumbnail;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Box;
use Imagine\Image\Point;

class CorrectThumbnailFilterLoader implements LoaderInterface
{
    /**
     * Constructor
     *
     * @param ImagineInterface $imagine
     */
    public function __construct(ImagineInterface $imagine)
    {
        $this->imagine = $imagine;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ImageInterface $image, array $options = array())
    {
        $mode = ImageInterface::THUMBNAIL_INSET;
        if (!empty($options['mode']) && 'inset' === $options['mode']) {
            $mode = ImageInterface::THUMBNAIL_INSET;
        }

        list($width, $height) = $options['size'];

        $size       = $image->getSize();
        $origWidth  = $size->getWidth();
        $origHeight = $size->getHeight();


        if (null === $width || null === $height) {
            if (null === $height) {
                $height = (int) (($width / $origWidth) * $origHeight);
            } else if (null === $width) {
                $width = (int) (($height / $origHeight) * $origWidth);
            }
        }

        $filter   = new Thumbnail(new Box($width, $height), $mode);
        $newImage = $filter->apply($image);

        $newSize = $newImage->getSize();
        $rWidth  = $newSize->getWidth();
        $rHeight = $newSize->getHeight();

        $xpos = (int) (($width - $rWidth) / 2);
        $ypos = (int) (($height - $rHeight) / 2);

        $background = $newImage->palette()->color(
            isset($options['color'])        ? $options['color']        : '#fff',
            isset($options['transparency']) ? $options['transparency'] : null
        );
        $topLeft    = new Point($xpos, $ypos);
        $canvas     = $this->imagine->create(new Box($width, $height), $background);

        return $canvas->paste($newImage, $topLeft);
    }
}