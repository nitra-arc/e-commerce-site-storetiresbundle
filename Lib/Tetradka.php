<?php

namespace Nitra\StoreBundle\Lib;

use Symfony\Component\DependencyInjection\Container;

class Tetradka
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;
    /** @var \Doctrine\Common\Cache\ApcCache */
    protected $cache;
    /** @var \Symfony\Bridge\Monolog\Logger */
    protected $logger;

    /**
     * Host name to tetradka
     * @var string
     */
    protected $host;

    /**
     * Constructor
     * @param string $tetradkaHost
     */
    public function __construct(Container $container, $tetradkaHost)
    {
        $this->container = $container;
        $this->cache     = $container->get('cache_apc');
        $this->logger    = $container->get('logger');
        $this->host      = $tetradkaHost;
    }

    /**
     * send order
     * @param array $orderData
     * @return array
     */
    public function sendOrder($orderData)
    {
        return $this->getData('create-order-from-site', $orderData);
    }

    /**
     * Getting currencies and cache him on 30 minutes
     * @return array
     */
    public function getCurrencies()
    {
        return $this->getData('currency/get-all-currencies', null, true, 60 * 30) ? : array();
    }

    /**
     * Get cities
     * @param boolean $groupByRegion
     * @return array
     */
    public function getCities($groupByRegion = true)
    {
        $Cities = $this->getData('city/get-all-cities', null, true) ? : array();

        if (!$groupByRegion) {
            return $Cities;
        }

        $cities = array();
        foreach ($Cities as $city) {
            $region = key_exists('region_name', $city) ? $city['region_name'] : null;
            if (!key_exists($region, $cities)) {
                $cities[$region] = array();
            }
            $cities[$region][$city['id']] = $city['city_name'];
        }
        foreach ($cities as $region => &$regionCities) {
            natcasesort($regionCities);
        }

        return $cities;
    }

    /**
     * Get delivery cities
     * @return array Delivery cities
     */
    public function getDeliveryCities()
    {
        return $this->getData('city/get-delivery-cities', null, true);
    }

    /**
     * Get nearest warehouse for product
     * @param string $productId Id of product
     * @return array Delivery cities
     */
    public function getNearestWarehouseForProduct($productId)
    {
        return $this->getData('order/get-closest-warehouse-for-product', http_build_query(array(
            'productId' => $productId,
        )));
    }

    /**
     * getting payment methods from tetradka
     * @return array
     */
    public function getPaymentMethods()
    {
        $methods = $this->getData('get-payment-site', null, true, 60 * 60) ? : array();
        $results = array();
        foreach ($methods as $method) {
            $results[$method['id']] = $method['name'];
        }

        return $results;
    }

    /**
     * getting data from tetradka (or cache)
     * @param string        $path           path (sample: 'city/get-all-cities')
     * @param array|null    $data           POST data
     * @param bool          $cache          use cache?
     * @param int           $cacheLifeTime  cache life time (in seconds, 24 hours by default - 60 * 60 * 24)
     */
    public function getData($path, $data = null, $cache = false, $cacheLifeTime = 86400)
    {
        $url = "http://{$this->host}/$path";
        $key = "nitra_tetradka_curl_$url";

        if ($cache && $this->cache->contains($key)) {
            return $this->cache->fetch($key);
        } else {
            $response = $this->sendRequest($url, $data);

            if ($cache && $response) {
                $this->cache->save($key, $response, $cacheLifeTime);
            }

            return $response;
        }
    }

    /**
     * sending request
     * @param string $url
     * @param array|null $data
     * @return mixed data returned from tetradka
     */
    protected function sendRequest($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($data) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        $curlResponse = curl_exec($ch);
        curl_close($ch);

        $this->logger->notice(sprintf("Nitra: tetradka - %s - %s - %s", $url, print_r($data, true), $curlResponse));

        return json_decode($curlResponse, true);
    }
}