<?php

namespace Nitra\StoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Nitra\StoreBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;
use Nitra\StoreBundle\DependencyInjection\Compiler\FormPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class NitraStoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideServiceCompilerPass());
        $container->addCompilerPass(new FormPass());
    }
}