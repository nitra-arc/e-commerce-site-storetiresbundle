(function ($) {
    $.fn.preloadImage = function () {
        return this.each(function () {
            var $this = $(this);
            var image = new Image();
            image.onload = function () {
                $this
                    .prop('src', $this.data('src'))
                    .removeAttr('style')
                    .addClass('preloaded');
            };
            image.src = $this.data('src');
        });
    };
})(jQuery);