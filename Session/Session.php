<?php

namespace Nitra\StoreBundle\Session;

use Symfony\Component\HttpFoundation\Session\Session as BaseSession;

class Session extends BaseSession
{
    /**
     * {@inheritdoc}
     */
    public function has($name)
    {
        if (isset($_COOKIE['PHPSESSID']) || session_status() === \PHP_SESSION_ACTIVE) {
            return parent::has($name);
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($name, $default = null)
    {
        if (isset($_COOKIE['PHPSESSID']) || session_status() === \PHP_SESSION_ACTIVE) {
            return parent::get($name, $default);
        } else {
            return $default;
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function remove($name)
    {
        $parent = parent::remove($name);
        if (!count($this->all())) {
            session_destroy();
            setcookie("PHPSESSID","",time()-3600,"/");
        } else {
            return $parent;
        }
    }
}