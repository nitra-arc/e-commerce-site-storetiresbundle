<?php

namespace Nitra\StoreBundle\Traits;

use Nitra\StoreBundle\Lib\Globals;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Alias Trait
 */
trait AliasDocument
{
    /**
     * @var string Алиас en для ссылки
     * @ODM\String
     */
    protected $aliasEn;

    /**
     * @var string Алиас ru для ссылки
     * @ODM\String
     */
    protected $aliasRu;

    /**
     * Set aliasEn
     * @param string $aliasEn
     * @return self
     */
    public function setAliasEn($aliasEn)
    {
        $this->aliasEn = $aliasEn;
        return $this;
    }

    /**
     * Get aliasEn
     * @return string $aliasEn
     */
    public function getAliasEn()
    {
        return $this->aliasEn;
    }

    /**
     * Set aliasRu
     * @param string $aliasRu
     * @return self
     */
    public function setAliasRu($aliasRu)
    {
        $this->aliasRu = $aliasRu;
        return $this;
    }

    /**
     * Get aliasRu
     * @return string $aliasRu
     */
    public function getAliasRu()
    {
        return $this->aliasRu;
    }

    /**
     * Get alias
     * @return string
     */
    public function getAlias()
    {
        $storeUrlLanguage   = Globals::getStoreUrlLanguage();
        $alias              = $this->{'getAlias' . ucwords($storeUrlLanguage)}();

        return $alias;
    }
}