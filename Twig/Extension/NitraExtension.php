<?php

namespace Nitra\StoreBundle\Twig\Extension;

use Symfony\Component\Translation\Translator;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Nitra\StoreBundle\Lib\Globals;

class NitraExtension extends \Twig_Extension
{
    /**
     * @var Translator Translator instance
     */
    protected $translator;

    /**
     * @var array Settings for format prices
     */
    protected $priceFormatSettings;

    /**
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * Constructor
     *
     * @param Translator    $translator
     * @param FilterManager $filterManager
     * @param array         $priceFormatSettings
     */
    public function __construct(Translator $translator, FilterManager $filterManager, $priceFormatSettings)
    {
        $this->translator          = $translator;
        $this->priceFormatSettings = $priceFormatSettings;
        $this->filterManager       = $filterManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nitra_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            'url_decode'            => new \Twig_Filter_Method($this, 'urlDecode'),
            'strpos'                => new \Twig_Filter_Method($this, 'strPos'),
            'preg_replace'          => new \Twig_Filter_Method($this, 'pregReplace'),
            'declension'            => new \Twig_Filter_Method($this, 'declension'),
            'price'                 => new \Twig_Filter_Method($this, 'price', array(
                'is_safe' => array('all',),
            )),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('liip_filter_data', array($this, 'liipFilterData')),
        );
    }

    /**
     * URL Decode a string
     *
     * @param string $url
     *
     * @return string The decoded URL
     */
    public function urlDecode($url)
    {
        return urldecode($url);
    }

    /**
     * Вхождение в строку подстроки
     *
     * @param string $string
     * @param string $needle
     *
     * @return boolean
     */
    public function strPos($string, $needle)
    {
        return strpos($string, $needle);
    }

    /**
     * Замена подстроки по регулярному выражению
     *
     * @param string $string        String for replace
     * @param array  $replacements  Array with replacements (<b>key</b> - what (regex), <b>value</b> - than)
     *
     * @return string
     */
    public function pregReplace($string, $replacements)
    {
        return preg_replace(
            array_keys($replacements),
            array_values($replacements),
            $string
        );
    }

    /**
     * Format price
     *
     * @param string $price
     * @param boolean $glue
     *
     * @return string
     */
    public function price($price, $glue = true)
    {
        $formatted = null;
        $decimals  = Globals::getPriceRound();
        $symbol    = Globals::getCurrency('symbol')
            ?: $this->translator->trans('currency', array(), 'NitraStoreBundle');

        if ($glue) {
            $formatted = number_format(
                $price,
                $decimals,
                $this->priceFormatSettings['dec_point'],
                $this->priceFormatSettings['thousands_sep']
            );
        }

        return $formatted . '<small>&thinsp;' . $symbol . '</small>';
    }

    /**
     * Склонение вывода в соосветствии с количеством.
     *
     * @param int    $count         количество
     * @param string $firstWord     один (товар)
     * @param string $secondWord    два-три (товара)
     * @param string $thirdWord     много (товаров)
     * @param bool   $glue          Склеивать ли результат с цифрой
     *
     * @return string
     */
    public function declension($count, $firstWord, $secondWord, $thirdWord, $glue = true)
    {
        $words = array($firstWord, $secondWord, $thirdWord);
        $cases = array(2, 0, 1, 1, 1, 2);
        $word = $words[
            (($count % 100) > 4 && ($count % 100) < 20)
                ? 2
                : $cases[min($count % 10, 5)]
            ];

        $result = null;
        if ($glue) {
            $result .= $count . ' ';
        }

        return $result . $word;
    }

    /**
     * Get data about imagine filter
     *
     * @param string $filterName
     * @return array
     */
    public function liipFilterData($filterName)
    {
        return $this->filterManager->getFilterConfiguration()->get($filterName);
    }
}